// On importe express
const express = require('express');
require('dotenv').config();


// Importation de mongoose
const mongoose = require('mongoose');

mongoose.connect(process.env.MONGO).then(() => console.log('database connected')).catch(err => { console.log(err)});

const hotelRoutes = require('./routes/hotel');
const flyRoutes = require('./routes/fly');
const cityCodeRoutes = require('./routes/cityCode');



const app = express();

app.use(express.json());

app.use('/api/hotel', hotelRoutes);
app.use('/api/fly', flyRoutes);
app.use('/api/cityCode', cityCodeRoutes);



module.exports = app;

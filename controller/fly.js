
const Fly = require('../models/fly');


exports.search = (req, res, next) => {
  console.log(req.query.dept);
  console.log(req.query.dateDept);
  console.log(req.query.dateRet);
  Fly.find({
      dept: req.query.dept,
      dest:req.query.dest,
       "flyStart.departure.at": { $gte:new Date(req.query.dateDept)},
       "flyArrival.departure.at": { $gte:new Date(req.query.dateRet)},
       "price.total": { $gte: req.query.priceMin, $lte: req.query.priceMax}
      //  flyArrival:{$elemMatch:{"arrival.at": { $gte:new Date(req.query.dateRet)}}}
    }).then(
      (fly) => {

          res.status(200).json(fly);
        
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  };
 
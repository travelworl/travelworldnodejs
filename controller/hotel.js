
const Hotel = require('../models/hotel');


exports.findHotelByName = (req, res, next) => {
  console.log(req.query.name);
  Hotel.find({
      cityCode: req.query.name,
      // offers: { $elemMatch: { checkInDate:{$lte:req.dateIn} , checkOutDate:{$gte:req.dateOut} ,guests: req.guests, "price.total": { $gte: req.priceMin, $lte: req.priceMax} } } 
      offers: { $elemMatch: { checkInDate:{$lte:req.query.dateIn} , checkOutDate:{$gte:req.query.dateOut},guests: req.query.guests,"price.total": { $gte: req.query.priceMin, $lte: req.query.priceMax}} } 

    }).then(
      (hotel) => {

          res.status(200).json(hotel);
        
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  };
 
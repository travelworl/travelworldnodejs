
const cityCodes = require('../models/cityCodes');


exports.find = (req, res, next) => {
 
    cityCodes.find({
      
    }).then(
      (cityCodes) => {

          res.status(200).json(cityCodes);
        
      }
    ).catch(
      (error) => {
        res.status(400).json({
          error: error
        });
      }
    );
  };
 
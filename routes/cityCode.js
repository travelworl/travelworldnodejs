const express = require('express');
const cityCodeCtrl = require('../controller/cityCode');
const router = express.Router();
router.get('/find',  cityCodeCtrl.find);
module.exports = router;

const express = require('express');
const flyCtrl = require('../controller/fly');
const router = express.Router();
router.get('/search',  flyCtrl.search);
module.exports = router;

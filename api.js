const express = require('express');
const app = express();
const { MongoClient } = require('mongodb');
const url = 'mongodb://127.0.0.1:27017';
const client = new MongoClient(url);
const dbName = 'travelWorld';
app.use(express.json())
app.get('/exa', async (req,res) => {
    await client.connect();
    const db = client.db(dbName);
    const collection = db.collection('hotels');
    const findResult = await collection.find({}).toArray();
    res.status(200).json(findResult);
})
app.listen(3112, () => {
        console.log("Rest API using express up")
})